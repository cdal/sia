# This repo contains the codes for Sequential Initialization Algorithm(SIA) from the paper "Model-based Clustering using Automatic Differentiation - Confronting Misspecification and High-Dimensional Data"(https://arxiv.org/pdf/2007.12786.pdf). 

**The Low Dimensional example demonstrates SIA on 4 component 2 dimensional Pinwheel Dataset. Plesae refer to Figures 5c and 5d from the paper for more details.**

**The High Dimensional example demonstrates SIA on 4 component 50 Dimensional GMM dataset. Please refer to table 3 from the paper. **

