import matplotlib.pyplot as plt

import autograd.numpy as np
import autograd.numpy.random as npr
from autograd import grad, hessian_vector_product
from scipy.optimize import minimize
from autograd.scipy.special import logsumexp
import autograd.scipy.stats.multivariate_normal as mvn
from autograd.misc.flatten import flatten_func
from data import make_pinwheel


import pandas as pd
import torch
import torch.nn as nn
import torch.utils.data as Data
import torchvision
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import numpy as np
import sys
from torch.autograd import Variable


from sklearn import mixture
import autograd.numpy as np
import autograd.numpy.random as npr


from data import make_pinwheel
import autograd.scipy.stats.multivariate_normal as mvn
















### First, we define a new logsumexp function

def logsumexp1(inputs, dim=None, keepdim=False):
    """Numerically stable logsumexp.

    Args:
        inputs: A Variable with any shape.
        dim: An integer.
        keepdim: A boolean.

    Returns:
        Equivalent of log(sum(exp(inputs), dim=dim, keepdim=keepdim)).
    """
    # For a 1-D array x (any array along a single dimension),
    # log sum exp(x) = s + log sum exp(x - s)
    # with s = max(x) being a common choice.
    if dim is None:
        inputs = inputs.view(-1)
        dim = 0
    s, _ = torch.max(inputs, dim=dim, keepdim=True)
    outputs = s + (inputs - s).exp().sum(dim=dim, keepdim=True).log()
    if not keepdim:
        outputs = outputs.squeeze(dim)
    return outputs




## We define new init_params function - one with Torch Tensors with a preset seed


def torch_init_gmm_params(num_components, D, scale):
    torch.manual_seed(0)
    return {'log proportions': torch.randn(num_components,dtype=torch.float, requires_grad = True) * scale,
            'means':           torch.randn(num_components, D,dtype=torch.float, requires_grad = True) * scale,
            'lower triangles': torch.randn(num_components, D, D, dtype=torch.float, requires_grad = True)*scale + torch.eye(D, dtype=torch.float, requires_grad =False)}

# print("params first \n",torch_init_gmm_params(3,2,0.1))

# Next we define a new GMM LL function with Torch

def torch_mvn_cov_logpdf(data, mu, cov_sqrt):
    #data1  = torch.from_numpy(data)
    data1 = data
    dim_data = data1.shape[1]
    mu1 = mu.float()
    sum_tensor = torch.zeros(data1.shape[0],dtype=torch.float64)
    cov = torch.mm(torch.transpose(cov_sqrt,0,1), cov_sqrt)
    for i in range(data1.shape[0]):
        x_mu_t = (data1[i]-mu1).reshape((dim_data,1))
        sum_tensor[i] = -0.5*torch.log(torch.det(2*np.pi*cov)) -0.5*torch.mm(  (torch.mm( torch.transpose(x_mu_t,0,1) , torch.inverse(cov) )), x_mu_t)
    return sum_tensor
    #return torch.sum(sum_tensor)    
    
    
# we define the remaining equivalent torch auxiliary functions

def torch_log_normalize(x):
    return x - logsumexp1(x)

def torch_unpack_gmm_params(params):
    normalized_log_proportions = torch_log_normalize(params['log proportions'])
    return normalized_log_proportions, params['means'], params['lower triangles']

def torch_gmm_log_likelihood(params, data):
    #cluster_lls = torch.zeros((data.shape[0],torch_init_gmm_params(4,2,0.1)['log proportions'].shape[0]),dtype=torch.float64)
    cluster_lls = torch.zeros((data.shape[0],params['log proportions'].shape[0]),dtype=torch.float)
    i = 0
    for log_proportion, mean, cov_sqrt in zip(*torch_unpack_gmm_params(params)):
        #cov = torch.mm(cov_sqrt.T, cov_sqrt)
        #cluster_lls[i]=torch_mvn_cov_logpdf(data, mean, cov_sqrt)
        cluster_lls[:,i]=(log_proportion + torch_mvn_cov_logpdf(data, mean, cov_sqrt))
        i = i+1
    #return cluster_lls    
    return torch.sum(logsumexp1(cluster_lls,dim = 1))
#     return np.sum(logsumexp(np.vstack(cluster_lls), axis=0))

def torch_objective(params):
        return -torch_gmm_log_likelihood(params, data)    
    
def torch_mvn_cov_logpdf(data, mu, cov_sqrt):
    if not isinstance(data, torch.Tensor):
         raise TypeError('data should be a torch.Tensor')
    #data1  = torch.from_numpy(data)
    data1 = data
    dim_data = data1.shape[1]
    mu1 = mu
    sum_tensor = torch.zeros(data1.shape[0],dtype=torch.float64)
    cov = torch.mm(torch.transpose(cov_sqrt,0,1), cov_sqrt)
    for i in range(data1.shape[0]):
        x_mu_t = (data1[i]-mu1).reshape((dim_data,1))
        sum_tensor[i] = -0.5*torch.log(torch.det(2*np.pi*cov)) -0.5*torch.mm(  (torch.mm( torch.transpose(x_mu_t,0,1) , torch.inverse(cov) )), x_mu_t)

    return sum_tensor


#### Part -2

##### In order to work with Torch's adam optimizer, we need to work with flattened parameters. So, we modify the  `torch_gmm_log_likelihood` to `torch_gmm_log_likelihood_v2`

def torch_gmm_log_likelihood_v1(logprop_params, mean_params, covsqrt_params, data):
    #cluster_lls = torch.zeros((data.shape[0],torch_init_gmm_params(4,2,0.1)['log proportions'].shape[0]),dtype=torch.float64)
    cluster_lls = torch.zeros((data.shape[0],logprop_params.shape[0]),dtype=torch.float)
    normalized_logprop = torch_log_normalize(logprop_params)
    for i in range(logprop_params.shape[0]) :
        #cov = torch.mm(cov_sqrt.T, cov_sqrt)
        #cluster_lls[i]=torch_mvn_cov_logpdf(data, mean, cov_sqrt)
        cluster_lls[:,i]=(normalized_logprop[i] + torch_mvn_cov_logpdf(data, mean_params[i], covsqrt_params[i]))
           
    return torch.sum(logsumexp1(cluster_lls,dim = 1))
def torch_gmm_log_likelihood_v2(flat_params, indices, values, data ):
        unflat_params = recover_flattened_v1(flat_params, indices, values)
        logprop_params = unflat_params[0]
        mean_params = unflat_params[1]
        covsqrt_params = unflat_params[2]
#         print(logprop_params, mean_params, covsqrt_params)
        return torch_gmm_log_likelihood_v1(logprop_params, mean_params, covsqrt_params, data)


### This is penalized objective
def torch_gmm_log_likelihood_v3(flat_params, indices, values, data, cov_hp1, cov_hp2, kl_hp1,kl_hp2 ):
    """
    This is the main penalized function - 
     Args:
        flat_params: flattened parameters
        indices: An integer.
        values: A boolean.
        data: Input data
        cov_hp1: Hyperparameter $w_2$
        cov_hp2: Hyperparameter $\lambda_i$ 
        kl_hp1: Hyperparameter for forward KL-div
        kl_hp2: Hyperparameter for reverse KL-div

    Returns:
        the penalized SIA loglikelihood
    """
    unflat_params = recover_flattened_v1(flat_params, indices, values)
    logprop_params = unflat_params[0]
    mean_params = unflat_params[1]
    covsqrt_params = unflat_params[2]
    #print(covsqrt_params.shape[0])
    cov_params = torch.zeros(covsqrt_params.shape[0],dtype=torch.float)
    for i in range(covsqrt_params.shape[0]):
        cov_params[i] = cov_hp1*( torch.log(torch.det(torch.mm(torch.transpose(covsqrt_params[i],0,1), covsqrt_params[i]).double()))  - cov_hp2).pow(2)
    cov_params_kl = covsqrt_params.clone()
    for i in range(covsqrt_params.shape[0]):
        cov_params_kl[i] = torch.mm(torch.transpose(covsqrt_params[i],0,1), covsqrt_params[i]).double()
    return -torch_gmm_log_likelihood_v1(logprop_params, mean_params, covsqrt_params, data) + torch.sum(cov_params) +\
            kl_hp1*kl_div_tot(mean_params,cov_params_kl) + kl_hp2*kl_div_inverse_tot(mean_params,cov_params_kl)  
    





def torch_objective_v1(logprop_params, mean_params, covsqrt_params):
        return -torch_gmm_log_likelihood_v1(logprop_params, mean_params, covsqrt_params, data)
def torch_objective_v2(flat_params, indices, values, data_input ):
        return -torch_gmm_log_likelihood_v2(flat_params, indices, values, data_input)   
    
    
    
    
def torch_objective_v3(flat_params, indices, values, data_input,cov_hp1, cov_hp2, kl_hp1,kl_hp2 ):
        return torch_gmm_log_likelihood_v3(flat_params, indices, values, data_input,cov_hp1,cov_hp2, kl_hp1,kl_hp2)            
    

    
##### In order to work flattened parameters, we define the function `flatten_params` and to recover the parameters back we define `recover_flattened_V1`.

# https://discuss.pytorch.org/t/how-to-flatten-and-then-unflatten-all-model-parameters/34730/2

def flatten_params(parameters):
    """
    flattens all parameters into a single column vector. Returns the dictionary to recover them
    :param: parameters: a generator or list of all the parameters
    :return: a dictionary: {"params": [#params, 1],
    "indices": [(start index, end index) for each param] **Note end index in uninclusive**

    """
    l = [torch.flatten(p) for p in parameters]
    indices = []
    s = 0
    for p in l:
        size = p.shape[0]
        indices.append((s, s+size))
        s += size
    flat = torch.cat(l).view(-1, 1)
    return {"params": flat, "indices": indices}


def recover_flattened(flat_params, indices, model):
    """
    Gives a list of recovered parameters from their flattened form
    :param flat_params: [#params, 1]
    :param indices: a list detaling the start and end index of each param [(start, end) for param]
    :param model: the model that gives the params with correct shapes
    :return: the params, reshaped to the ones in the model, with the same order as those in the model
    """
    l = [flat_params[s:e] for (s, e) in indices]
    for i, p in enumerate(model.parameters()):
        l[i] = l[i].view(*p.shape)
    return l


def recover_flattened_v1(flat_params, indices, model):
    """
    Gives a list of recovered parameters from their flattened form
    :param flat_params: [#params, 1]
    :param indices: a list detaling the start and end index of each param [(start, end) for param]
    :param model: the model that gives the params with correct shapes
    :return: the params, reshaped to the ones in the model, with the same order as those in the model
    """
    l = [flat_params[s:e] for (s, e) in indices]
    for i, p in enumerate(model):
        l[i] = l[i].view(*p.shape)
    return l

import copy
def recover_flattened_v3(flat_params, indices, model):
    """
    Gives a list of recovered parameters from their flattened form
    :param flat_params: [#params, 1]
    :param indices: a list detaling the start and end index of each param [(start, end) for param]
    :param model: the model that gives the params with correct shapes
    :return: the params, reshaped to the ones in the model, with the same order as those in the model
    """
    l = [flat_params[s:e] for (s, e) in indices]
    for i, p in enumerate(model):
        
#         print(i,p)
        l[i] = l[i].view(*model[p].shape)
        #print(str(p), model[str(p)], type(l))
        model_1 = model
        #model_1 = copy.deepcopy(model)
        #print("before",model_1)
        model_1[p] = l[i].view(*model[p].shape).detach().numpy()
        #print("after", model_1)
        #model_1[p] = model_1.pop(i)
#     dict((d1[key], value) for (key, value) in d.items())    
   
    
    
#     return l, model_1, model
    return model_1



### redefine the `torch_init_gmm_params` to be of float.64 type

def torch_init_gmm_params(num_components, D, scale):
    torch.manual_seed(0)
    return {'log proportions': torch.randn(num_components,dtype=torch.float64, requires_grad = True) * scale,
            'means':           torch.randn(num_components, D,dtype=torch.float64, requires_grad = True) * scale,
#             'lower triangles': torch.randn(num_components, D, D, dtype=torch.float64, requires_grad = True)*scale }
            'lower triangles': torch.randn(num_components, D, D, dtype=torch.float64, requires_grad = True)*scale + torch.eye(D, dtype=torch.float64, requires_grad =True)}

# print("params second \n",torch_init_gmm_params(3,2,0.1))




def kl_mvn(m0, S0, m1, S1):
   
    # store inv diag covariance of S1 and diff between means
    N = m0.shape[0]
    iS1 = torch.inverse(S1)
    diff = (m1 - m0).reshape((N,1))
    #print("The value of N", N)
    # kl is made of three terms
    tr_term   = torch.trace(iS1 @ S0)
    det_term  = torch.log(torch.det(S1)) - torch.log(torch.det(S0)) #torch.sum(torch.log(S1)) - torch.sum(torch.log(S0))
    quad_term = torch.transpose(diff,0,1) @ torch.inverse(S1) @ diff #torch.sum( (diff*diff) * iS1, axis=1)
    #print("det term {}",format(det_term))
    return .5 * (tr_term + det_term + quad_term - N) 

def kl_inverse_mvn(m1, S1, m0, S0):
    """
    Kullback-Liebler divergence from Gaussian pm,pv to Gaussian qm,qv.
    Also computes KL divergence from a single Gaussian pm,pv to a set
    of Gaussians qm,qv.
    Diagonal covariances are assumed.  Divergence is expressed in nats.

    - accepts stacks of means, but only one S0 and S1

    From wikipedia
    KL( (m0, S0) || (m1, S1))
         = .5 * ( tr(S1^{-1} S0) + log |S1|/|S0| + 
                  (m1 - m0)^T S1^{-1} (m1 - m0) - N )
    """
    # store inv diag covariance of S1 and diff between means
    N = m0.shape[0]
    iS1 = torch.inverse(S1)
    diff = (m1 - m0).reshape((N,1))

    # kl is made of three terms
    tr_term   = torch.trace(iS1 @ S0)
    det_term  = torch.log(torch.det(S1)) - torch.log(torch.det(S0)) #torch.sum(torch.log(S1)) - torch.sum(torch.log(S0))
    quad_term = torch.transpose(diff,0,1) @ torch.inverse(S1) @ diff #torch.sum( (diff*diff) * iS1, axis=1)
    #print(tr_term,det_term,quad_term)
    return .5 * (tr_term + det_term + quad_term - N) 


def kl_div_tot(means,covariances):
    kl_divs = torch.zeros((means.shape[0],means.shape[0]),dtype=torch.float)
    for i in range(0,means.shape[0]):
        #print(i)
        for j in range(i,means.shape[0]):
            #print(i,j)
            #print(means[i],covariances[i])
            kl_divs[i,j] = kl_mvn(means[i],covariances[i],means[j],covariances[j]) 
    #print(kl_divs) 
    #print(" The value of kl_div_tot ", kl_divs)
    return (torch.sum(kl_divs))

def kl_div_inverse_tot(means,covariances):
    kl_divs = torch.zeros((means.shape[0],means.shape[0]),dtype=torch.float)
    for i in range(0,means.shape[0]):
        #print(i)
        for j in range(i,means.shape[0]):
            #print(i,j)
            kl_divs[i,j] = kl_inverse_mvn(means[i],covariances[i],means[j],covariances[j])
    #print(kl_divs)
    #print(" The value of kl_div_inverse_tot ", kl_divs)
    return (torch.sum(kl_divs))


def kl_div_tot_print(means,covariances):
    kl_divs = torch.zeros((means.shape[0],means.shape[0]),dtype=torch.float)
    for i in range(0,means.shape[0]):
        #print(i)
        for j in range(i,means.shape[0]):
            #print(i,j)
            kl_divs[i,j] = kl_mvn(means[i],covariances[i],means[j],covariances[j])
    print(kl_divs) 
    print(torch.sum(kl_divs))
    #return (torch.sum(kl_divs))

def kl_div_inverse_tot_print(means,covariances):
    
    kl_divs = torch.zeros((means.shape[0],means.shape[0]),dtype=torch.float)
    for i in range(0,means.shape[0]):
        #print(i)
        for j in range(i,means.shape[0]):
            #print(i,j)
            kl_divs[i,j] = kl_mvn(means[i],covariances[i],means[j],covariances[j])
    print(kl_divs) 
    
    
    
    
def py_kl_mvn(m0, S0, m1, S1):
   
    # store inv diag covariance of S1 and diff between means
    N = m0.shape[0]
    iS1 = np.linalg.inv(S1)
    diff = m1 - m0

    # kl is made of three terms
    tr_term   = np.trace(iS1 @ S0)
    det_term  = np.log(np.linalg.det(S1)) - np.log(np.linalg.det(S0)) #np.sum(np.log(S1)) - np.sum(np.log(S0))
    quad_term = diff.T @ np.linalg.inv(S1) @ diff #np.sum( (diff*diff) * iS1, axis=1)
    #print("det term {}",format(det_term))
    return .5 * (tr_term + det_term + quad_term - N) 

def py_kl_inverse_mvn(m1, S1, m0, S0):
    """
    Kullback-Liebler divergence from Gaussian pm,pv to Gaussian qm,qv.
    Also computes KL divergence from a single Gaussian pm,pv to a set
    of Gaussians qm,qv.
    Diagonal covariances are assumed.  Divergence is expressed in nats.

    - accepts stacks of means, but only one S0 and S1

    From wikipedia
    KL( (m0, S0) || (m1, S1))
         = .5 * ( tr(S1^{-1} S0) + log |S1|/|S0| + 
                  (m1 - m0)^T S1^{-1} (m1 - m0) - N )
    """
    # store inv diag covariance of S1 and diff between means
    N = m0.shape[0]
    iS1 = np.linalg.inv(S1)
    diff = m1 - m0

    # kl is made of three terms
    tr_term   = np.trace(iS1 @ S0)
    det_term  = np.log(np.linalg.det(S1)) - np.log(np.linalg.det(S0)) #np.sum(np.log(S1)) - np.sum(np.log(S0))
    quad_term = diff.T @ np.linalg.inv(S1) @ diff #np.sum( (diff*diff) * iS1, axis=1)
    #print(tr_term,det_term,quad_term)
    return .5 * (tr_term + det_term + quad_term - N) 


def py_kl_div_tot(means,covariances):
    kl_divs = []
    for i in range(0,means.shape[0]):
        #print(i)
        for j in range(i,means.shape[0]):
            #print(i,j)
            kl_divs.append(py_kl_mvn(means[i],covariances[i],means[j],covariances[j]) )
    #print(kl_divs)        
    return (np.sum(kl_divs))

def py_kl_div_inverse_tot(means,covariances):
    kl_divs = []
    for i in range(0,means.shape[0]):
        #print(i)
        for j in range(i,means.shape[0]):
            #print(i,j)
            kl_divs.append(py_kl_inverse_mvn(means[i],covariances[i],means[j],covariances[j]) )
    #print(kl_divs)        
    return (np.sum(kl_divs))


def py_kl_div_tot_print(means,covariances):
    kl_divs = []
    for i in range(0,means.shape[0]):
        #print(i)
        for j in range(i,means.shape[0]):
            #print(i,j)
            kl_divs.append(py_kl_mvn(means[i],covariances[i],means[j],covariances[j]) )
    print(kl_divs) 
    print(np.sum(kl_divs))
    #return (np.sum(kl_divs))

def py_kl_div_inverse_tot_print(means,covariances):
    kl_divs = []
    for i in range(0,means.shape[0]):
        #print(i)
        for j in range(i,means.shape[0]):
            #print(i,j)
            kl_divs.append(py_kl_inverse_mvn(means[i],covariances[i],means[j],covariances[j]) )
    print(kl_divs) 
    print(np.sum(kl_divs))   
    
    
    
    
    
    
    
    
    
    
def print_mpkl(means,covariances):
    kl_divs = []
    for i in range(0,means.shape[0]):
        #print(i)
        for j in range(i,means.shape[0]):
            #print(i,j)
            kl_divs.append(abs(py_kl_mvn(means[i],covariances[i],means[j],covariances[j]) - py_kl_inverse_mvn(means[i],covariances[i],means[j],covariances[j]) )  )
    return (kl_divs,np.max(kl_divs))    






 ### Below are Numpy functions
def init_gmm_params(num_components, D, scale):
    torch.manual_seed(0)
    return {'log proportions': torch.randn(num_components, requires_grad = True).cpu().detach().numpy() * scale,
            'means':           torch.randn(num_components, D).cpu().detach().numpy() * scale,
            'lower triangles': torch.randn(num_components, D, D, requires_grad = True).cpu().detach().numpy()*scale + torch.eye(D, requires_grad =True).cpu().detach().numpy()}


def log_normalize(x):
    return x - logsumexp(x)

def unpack_gmm_params(params):
    normalized_log_proportions = log_normalize(params['log proportions'])
    return normalized_log_proportions, params['means'], params['lower triangles']

def gmm_log_likelihood(params, data):
    cluster_lls = []
    for log_proportion, mean, cov_sqrt in zip(*unpack_gmm_params(params)):
        cov = np.dot(cov_sqrt.T, cov_sqrt)
        cluster_lls.append(log_proportion + mvn.logpdf(data, mean, cov))
    return np.sum(logsumexp(np.vstack(cluster_lls), axis=0))


def objective(params):
        return -gmm_log_likelihood(params, data)   


